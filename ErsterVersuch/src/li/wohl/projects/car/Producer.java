package li.wohl.projects.car;

public class Producer {
	
	private String name;
	private double percentage;
	
	public Producer (String name, double percentage) {
		super();
		this.name = name;
		this.percentage = percentage;
	};
	
	public double getPercentage() {
		return percentage;
	}

	public void setPercentage(double percentage) {
		this.percentage = percentage;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	 
	
}
