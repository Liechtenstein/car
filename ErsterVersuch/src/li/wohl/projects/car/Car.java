package li.wohl.projects.car;

public class Car {

	private String color;
	private String model;
	private Engine engine;
	private float maxspeed;
	private double baseprice;
	private double basemilage;
	private Producer producer; 
	private double kilometers;
	private String type;
	
	
	public Car(String color, String model, Engine engine, float maxspeed, double baseprice, double basemilage, Producer producer, double kilometers) {
		super();
		this.color = color;
		this.model = model;
		this.engine = engine;
		this.maxspeed = maxspeed;
		this.baseprice = baseprice;
		this.basemilage = basemilage;
		this.producer = producer;
		this.kilometers = kilometers;
	}

	public String getType() {
		this.type = this.engine.getType();
		return type;
	}

	public double getUsage() {
		getType();
		double usage = this.basemilage;
		if (this.type == "Benzin" && this.kilometers >= 50000 ) {
			usage = this.kilometers * 1.098;
		}
		return usage;
	}
	
	
	public double getPrice() {
		double price = this.baseprice - this.baseprice * this.producer.getPercentage();
		return price;
	}

	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}


	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}


	public Engine getEngine() {
		return engine;
	}
	public void setEngine(Engine engine) {
		this.engine = engine;
	}


	public float getMaxspeed() {
		return maxspeed;
	}
	public void setMaxspeed(float maxspeed) {
		this.maxspeed = maxspeed;
	}


	public double getBaseprice() {
		return baseprice;
	}
	public void setBaseprice(double baseprice) {
		this.baseprice = baseprice;
	}


	public double getBasemilage() {
		return basemilage;
	}
	public void setBasemilage(double basemilage) {
		this.basemilage = basemilage;
	}

	public Producer getProducer() {
		return producer;
	}

	public void setProducer(Producer producer) {
		this.producer = producer;
	}

	public double getKilometers() {
		return kilometers;
	}

	public void setKilometers(double kilometers) {
		this.kilometers = kilometers;
	}

	public void setType(String type) {
		this.type = type;
	}

	

	
}
