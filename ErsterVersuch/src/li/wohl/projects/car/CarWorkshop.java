package li.wohl.projects.car;

public class CarWorkshop {

	public static void main(String[] args) {
		
		Engine e1 = new Engine(150, "diesel");
		Engine e2 = new Engine(100, "benzin");
		
		Producer m1 = new Producer("Audi", 0.10); //Audi 10% Rabatt
		
		Car c1 = new Car("white", "A3", e1, 180, 10000, 700, m1, 40000);
		
		c1.getEngine().setHorsePower(250);

		Person p1 = new Person("Andre","Schmolmueller",18);
		
		p1.addCar(c1);
		
		System.out.println(p1.getValueofCars());
		System.out.println(c1.getColor());
		System.out.println(c1.getModel());
		System.out.println(c1.getEngine().getHorsePower());
		System.out.println(c1.getMaxspeed());
		System.out.println(c1.getBaseprice());
		System.out.println(c1.getBasemilage());
		System.out.println(c1.getProducer().getPercentage());
		System.out.println(c1.getKilometers());
	}
}
