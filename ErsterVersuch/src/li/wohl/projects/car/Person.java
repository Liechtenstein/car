package li.wohl.projects.car;

import java.util.ArrayList;

import java.util.List;

import org.joda.time.Years;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Person {

	private String firstName, lastName;
	private Date birthday;
	private List<Car> cars;	
	
	
	public Person(String firstName, String lastName, String day, String month, String year ) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.cars = new ArrayList<>();
		
		
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
		
		String d = day + month + year;
		
		try {
			Date birthdate = sdf.parse(d);
			this.birthday = birthdate;
		} catch (ParseException e) {
			//Todo auto generated catch back
			this.birthday = new Date();
		}
	}
	
	public int getAge() {
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
		Date today = new Date();
		
		DateTime dtBirthDay = new DateTime(brithday);
		DateTime dtToday = new DateTime(today);
		
		int years = Years.yearsBetween(dtBirthday, dtToday).getYears();
		return years;
	}

	
	
	public double getValueofCars(){
		double ValueofCars = 0;
		for (Car car : cars) {
			ValueofCars = car.getPrice() + ValueofCars;
		}
		return ValueofCars;
	}

	public void addCar(Car c) {
		this.cars.add(c);
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	


	public List<Car> getCars() {
		return cars;
	}

	public void setCars(List<Car> cars) {
		this.cars = cars;
	}

}

